From ubuntu:14.04

MAINTAINER ELBV <eloi.le-bastart-de-villeneuve@openergy.fr>

RUN apt-get update && install vim -d cd

RUN apt-get install -y tar git curl nano wget dialog net-tools build-essential

RUN apt-get install -y python python-dev python-distribute python-pip


COPY requirements.txt /tmp/
RUN pip install --requirement /tmp/requirements.txt
COPY . /tmp/


COPY example.py /

EXPOSE 5000

CMD ["python", "/example.py"]
