-----------------------------------------------------------------------
19-May-2016
OPENERGY - Docker dive-in tutorial
Eloi Le
------------------------------------------------------------------------

.:: INTRODUCTION ::.

This a tutorial/stick note towards running a simple process in docker,
which consists in building an image containing a simple flask-built python
web-app, and show it in the host browser.

We'll have all our files recorded in a testdocker order on the Desktop : up
to the reader to adapt the path to its system.

.:: REQUIREMENTS ::.

*******docker (1.11 or any boot2dock deprecated version)******
*******a dockerfile*******
From ubuntu:14.04
RUN apt-get update && install vim -d cd
RUN apt-get install -y tar git curl nano wget dialog net-tools build-essential
RUN apt-get install -y python python-dev python-distribute python-pip
COPY requirements.txt /tmp/
RUN pip install --requirement /tmp/requirements.txt
COPY . /tmp/
COPY example.py /
EXPOSE 5000
CMD ["python", "/example.py"]

*******requirements.txt*******
flask

*******example.py*******
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World!'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)


.:: PROCESSING ::.

	docker-machine start machine_name
- build the target image by typing :
	docker build -t build_name ../Desktop/testdocker/.
(. refers to the Dockerfile, as long as it's named Dockerfile)

	docker run -itdP build_name 
i and t are compulsory, don't ask me why
P randomly assigns a port forwarding. You may choose it yourself with 
  -p 7000:5000 for instance.

	docker ps
STATUS              PORTS                     NAMES
Up About a minute   0.0.0.0:32768->5000/tcp   gloomy_brattain

- Note down the number that's in 32768 position
	docker-machine url machine_name
tcp://192.168.99.100:2376

- And then combine the url with the port you got earlier. Here, this would
be http://192.168.199.100:32768

- Open your browser and call this url

- Check the answer is Hello world !	
.:: END ::.